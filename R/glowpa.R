run <- new.env()

#' Initialize GloWPa
#'
#' @description Function to reset the model run environment, validate the
#' provided model settings and input data, setup the logger, set the modelling
#' isoraster, isodata, pathogen data and correct the gridded population data.
#' The function should be run before the start of a new model run.
#'
#' @param file path to glowpa model settings file
#'
#' @return no return, but sets the internal model run environment.
#' @export
#'
#' @examples
#' example_settings <- system.file("extdata/kla", "kla_rotavirus.yaml",
#'   package = "glowpa"
#' )
#' glowpa_init(example_settings) # get the mopel run environment
#' model_run_env <- glowpa_get_run()
glowpa_init <- function(file) {
  # clear the model run environment
  for (entry in names(run)) {
    run[[entry]] <- NULL
  }
  # set the default settings in run environment
  run$settings <- defaults
  # load the user model settings and merge with defaults
  settings_load(file)
  # setup logger from user settings
  logger_init(run$settings$logger)
  # log the license
  logger_log_license()

  logger_log(sprintf("Current working directory: %s", getwd()))
  logger_log(sprintf("Use settings from %s", file))
  # load the pathogen user input data and set pathogen to run environment
  pathogen_init(run$settings)
  # log the model run settings
  settings_display(run$settings)
  # validate settings and input data
  validate()
  # set the domain
  domain_set(run$settings)
  # validate geometries
  validate_geometries(run$domain, run$settings)
  # read and set isodata
  isodata_set(run$settings$input$isodata)
  # validate iso codes from different sources
  validate_isocodes()
  # setup emissions data structure in model run environment
  init_emissions()
  # load and correct population data
  population_set(run$settings$input$population)
  logger_log("GloWPa successfully initialized", logger::SUCCESS)
}

#' Run GloWPa
#'
#' @description This function will start the GloWPa model and will simulate the
#' pathogen pathways from various sources to land and surface water. The model
#' run will produce the output files in the output directory specified in the
#' model settings. The in-memory pathogen emissions can also be requested from
#' the model run environment for direct post-processing without reading output
#' files.
#'
#' @return no return, but populates the model run environment. The model run
#'   progress will be logged to the console and/or file if the logger is
#'   enabled.
#' @export
#'
#' @examples
#' example_settings <- system.file("extdata/kla", "kla_rotavirus.yaml",
#'   package = "glowpa"
#' )
#' glowpa_init(example_settings)
#' glowpa_start()
#' glowpa_run <- glowpa_get_run()
#' list.files(glowpa_run$settings$output$dir, pattern = ".tif|.csv")
#' terra::plot(log10(glowpa_run$emissions$pathways$rast),
#'   col =
#'     hcl.colors(50, palette = "Geyser")
#' )
glowpa_start <- function() {
  logger_log("Start GloWPa simulation", logger::SUCCESS)
  run$timing <- list()
  run$timing$start <- Sys.time()
  # return the emissions from the sanitation to land, surface water, and waste
  # water treatment
  df_human_emissions <- human_emissions()
  # update the global emissions structure from human sources
  pathways_humans(df_human_emissions)
  pathways_humans_rast(df_human_emissions)

  rast_livestock_emissions <- NULL

  if (run$settings$livestock$enabled) {
    rast_livestock_emissions <- livestock_emissions()
    pathways_livestock(rast_livestock_emissions)
    # also accounts for manure storage to land
    pathways_livestock_df(rast_livestock_emissions)
  }

  use_hydrology <- run$settings$hydrology$enabled

  sds_to_land <- c(terra::sds(run$emissions$pathways$rast$humans2land))
  names(sds_to_land) <- c("humans2land")
  # add the emissions to land from the livestock module
  if (!is.null(rast_livestock_emissions)) {
    sds_livestock2land <-
      terra::sds(rast_livestock_emissions[c("livestock2land", "storage2land")])
    sds_to_land <- c(sds_to_land, sds_livestock2land)
  }
  list_out_land <- land_emissions(sds_to_land)
  # update the pathways table and return the total loads to surface water
  rast_land2water <- pathways_land(list_out_land)
  # calculate emissions from land to water using avg daily runoff per month
  if (use_hydrology) {
    rast_river_surival <- river_survival()
    # monthly loads from humans direct to surface water
    rast_humans2water <- run$emissions$pathways$rast$humans2water / 12
    # monthly loads from waste water treatment plants to surface water
    rast_wwtp2water <- run$emissions$pathways$rast$wwtp2water / 12
    # add the monthly loads from humans and wwtp to surface runoff
    rast_river_inflow <-
      sum(rast_land2water, rast_humans2water, rast_wwtp2water, na.rm = TRUE)
    rast_flowdir <- input_read_rast(run$settings$input$routing$flowdir)
    rast_flowacc <- input_read_rast(run$settings$input$routing$flowacc)
    rast_river_loads <-
      routing(rast_river_inflow,
              rast_river_surival,
              rast_flowdir,
              rast_flowacc)
    # clear memory
    rm(rast_flowdir, rast_flowacc)
    # update the hydrology output
    run$emissions$hydrology$loads <- rast_river_loads
  }

  output_set_meta()
  output_write(run$emissions, run$settings$output)

  run$timing$end <- Sys.time()
  logger_log("Finished GloWPa simulation", logger::SUCCESS)
}


#' GloWPa run environment
#'
#' @description Returns the internal environment of the last model run which
#' holds the model settings, in-memory input data, model state and output.
#'
#' @return R environment containing the GloWPa model run environment
#' @export
#'
#' @examples
#' example_settings <- system.file("extdata/kla", "kla_rotavirus.yaml",
#'   package = "glowpa"
#' )
#' glowpa_init(example_settings)
#' glowpa_start()
#' model_run <- glowpa_get_run()
glowpa_get_run <- function() {
  return(run)
}

#' GloWPa set run
#'
#' @description
#' Update the internal run environment. Experimental and not recommended.
#'
#' @param new_run Updated model run environment.
#'
#' @return void
#' @export
#'
#' @examples
#' model_run <- glowpa_get_run()
#' model_run$settings$logger$enabled <- FALSE
#' glowpa_set_run(model_run)
glowpa_set_run <- function(new_run) {
  for (entry in names(new_run)) {
    run[[entry]] <- new_run[[entry]]
  }
}
