settings_load <- function(file) {
  # validate the file
  is_yaml <- configr::is.yaml.file(file, eval.expr = TRUE)
  if (!is_yaml) {
    msg <- "GloWPa settings must be a valid yaml file"
    stop(msg)
  }
  glowpa_settings <- configr::read.config(file, eval.expr = TRUE)
  settings_apply(glowpa_settings)
}

settings_restore <- function() {
  logger_log("Restore default GloWPa settings.", logger::INFO)
  settings_apply(defaults)
}

settings_apply <- function(glowpa_settings) {
  global_settings <- utils::modifyList(defaults, glowpa_settings)
  run$settings <- global_settings
}

settings_display <- function(settings) {
  settings_str <- yaml::as.yaml(settings)
  logger_log(paste("Use settings:\n", settings_str), logger::INFO)
}
