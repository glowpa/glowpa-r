river_survival <- function() {
  # read monthly river water temperature
  rast_triver <-
    input_rast_steps(run$settings$input$hydrology$river_temperature, "monthly")
  # We assume that survival in waters below 4 degrees Celsius is the same as at
  # 4 degrees
  rast_triver[rast_triver < 4] <- 4

  # calculate decay rate from temperature related decay
  rast_kt <-
    terra::app(
      rast_triver,
      river_temperature_survival,
      k = run$pathogen$Kt,
      lambda = run$pathogen$lambda
    )

  # read mean DOC concentration
  rast_cdoc <- input_read_rast(run$settings$input$hydrology$doc)
  rast_rdepth_monmean <-
    input_rast_steps(run$settings$input$hydrology$river_depth, "monthly")
  rast_ssrd_monmean <-
    input_rast_steps(run$settings$input$hydrology$ssrd, "monthly")
  kl_input <-
    terra::sds(list(
      doc = rep(rast_cdoc, 12),
      rdepth = rast_rdepth_monmean,
      ssrd = rast_ssrd_monmean
    ))
  # calculate decay rate from radiation
  rast_kl <- terra::lapp(kl_input, function(cdoc, rdepth, ssrd, kl, kd) {
    kl_survival <- river_radiation_survival(ssrd, cdoc, rdepth, kl, kd)
  }, kl = run$pathogen$kl, kd = run$pathogen$kd)

  # calculate decay rate from sedimentation
  rast_ks <-
    river_sedimentation_survival(rast_rdepth_monmean, run$pathogen$v_settling)

  # read residence time (months -> days)
  rast_river_restime <-
    input_rast_steps(run$settings$input$hydrology$river_restime, "monthly") *
    lubridate::days_in_month(1:12)
  rast_k <- sum(rast_kt, rast_kl, rast_ks, na.rm = TRUE)

  rast_river_survival <- exp(-rast_k * rast_river_restime)
  return(rast_river_survival)
}

#' Temperature-dependent survival
#' @description
#' Calculate the temperature-dependent survival during transport with rivers following the approach by
#' \insertCite{PENG2008}{glowpa}
#'
#' @param triver numeric River temperature (Celcius)
#' @param lambda numeric constant (-)
#' @param k numeric decay rate coefficient at 4 degrees Celcius (day -1)
#'
#' @return numeric decay rate (day-1)
#' @importFrom Rdpack reprompt
#' @references
#' \insertRef{VERMEULEN2019}{glowpa}
#'
#' \insertRef{PENG2008}{glowpa}
#'
river_temperature_survival <- function(triver, lambda, k) {
  kt_survival <- k * exp(lambda * (triver - 4))
  return(kt_survival)
}

#' Solar radiation-dependent survival
#'
#' @description
#' Pathogen decay due to the ultraviolet (UV) part of solar radiation.
#'
#' @param ssrd numeric solar radiation (W m-2)
#' @param cdoc numeric dissolved organic carbon (DOC) concentration (mg L-1)
#' @param rdepth numeric water depth (m)
#' @param kl numeric constant (m2 kJ-1)
#' @param kd numeric constant (L mg-1 m-1)
#'
#' @return numeric decay rate (day-1)
#'
river_radiation_survival <- function(ssrd, cdoc, rdepth, kl, kd) {
  # convert W/m2 to kJ/m2
  irradiance <- ssrd * (60 * 60 * 24) / 1000
  kl_survival <-
    (kl * irradiance) / (kd * cdoc * rdepth) * (1 - exp(-kd * cdoc * rdepth))
  return(kl_survival)
}

#' Sedimentation-dependent survival
#' @description
#' Calculates the loss of pathogen loads due to sedimentation processes.
#'
#'
#' @param rdepth river depth (m)
#' @param v_settling settling velocity (m day-1). A typical value is around 0.1 m day-1.
#'
#' @return numeric decay rate (day-1)
#'
river_sedimentation_survival <- function(rdepth, v_settling) {
  ks_survival <- v_settling / rdepth
  return(ks_survival)
}
