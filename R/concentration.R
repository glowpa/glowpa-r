concentration <- function(rast_river_loads,
                          rast_discharge,
                          threshold_discharge = 1) {
  # grid cells with average discharge of less than 1 m3/s will be changed to zero
  # discharge this is done because otherwise the model calculates extremely high
  # concentrations for extremely dry and sparsely populated regions such as the
  # Sahara the model cannot be considered representative for such regions, so
  # better not include them.
  rast_discharge[rast_discharge < threshold_discharge] <- NA
  sec_in_day <- 60 * 60 * 24
  days_in_month <- 30
  if(terra::has.time(rast_discharge)){
    month_vals <- terra::time(rast_discharge)
    # overrule default
    days_in_month <- lubridate::days_in_month(month_vals)
  }
  # compute total monthly volume in m3/month
  rast_volume <-
    rast_discharge * days_in_month * sec_in_day

  rast_concentration <- rast_river_loads / rast_volume
  return(rast_concentration)
}
