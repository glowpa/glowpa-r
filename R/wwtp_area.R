wwtp_area_run <- function(df_emissions_sanitation) {
  iso <-
    area_type <-
    sanitation_type <-
    out_sewerage <- out_fecalSludge <- to_wwtp <- NULL
  logger_log("Start area based waste water treatment", logger::INFO)

  # returns the emissions out the fecalSludge and sewerage after treatment for
  # each sanitation type
  df_emissions_sanitation <- wwtp_area_treatment(df_emissions_sanitation)

  df_emissions_wwtp <- df_emissions_sanitation %>%
    dplyr::select(
      iso, area_type, sanitation_type, out_sewerage, out_fecalSludge, to_wwtp
    )

  pathways_wwtp_rast(df_emissions_wwtp)

  return(df_emissions_wwtp)
}

wwtp_area_treatment <- function(df_emissions_sanitation) {
  # supress linter warnings
  iso <- to_sewerage <- to_fecalSludge <- emitted_fraction <- NULL

  df_emissions_sanitation <-
    dplyr::left_join(df_emissions_sanitation,
      run$isodata %>% dplyr::select(iso, emitted_fraction),
      by = "iso"
    ) %>%
    dplyr::rowwise() %>%
    dplyr::mutate(
      out_sewerage = to_sewerage * emitted_fraction,
      out_fecalSludge = to_fecalSludge * emitted_fraction,
      to_wwtp = sum(to_sewerage, to_fecalSludge, na.rm = TRUE)
    )

  return(df_emissions_sanitation)
}

wwtp_area_emissions_to_grid <- function(df_emissions_wwtp) {
  # suppress linter warnings
  iso <- area_type <- pop_urban <- pop_rural <- NULL
  df_population <- run$isodata %>%
    dplyr::select(iso, pop_urban, pop_rural)

  # suppress linter warnings
  out_sewerage <-
    out_fecalSludge <-
    to_wwtp <- wwtp2water_pp <- humans2wwtp_pp <- NULL
  df_wwtp <- df_emissions_wwtp %>%
    dplyr::group_by(iso, area_type) %>%
    dplyr::summarise(
      wwtp2water = sum(out_sewerage, out_fecalSludge, na.rm = TRUE),
      humans2wwtp = sum(to_wwtp, na.rm = TRUE)
    ) %>%
    dplyr::group_by(area_type) %>%
    dplyr::mutate(
      wwtp2water_pp = dplyr::case_when(
        area_type == "urban" ~ wwtp2water / df_population %>%
          dplyr::select(pop_urban) %>%
          dplyr::pull(),
        area_type == "rural" ~ wwtp2water / df_population %>%
          dplyr::select(pop_rural) %>%
          dplyr::pull(),
        .default = NA
      ),
      humans2wwtp_pp = dplyr::case_when(
        area_type == "urban" ~ humans2wwtp / df_population %>%
          dplyr::select(pop_urban) %>%
          dplyr::pull(),
        area_type == "rural" ~ humans2wwtp / df_population %>%
          dplyr::select(pop_rural) %>%
          dplyr::pull(),
        .default = NA
      )
    ) %>%
    dplyr::mutate(
      wwtp2water_pp = replace(wwtp2water_pp, !is.finite(wwtp2water_pp), 0),
      humans2wwtp_pp = replace(humans2wwtp_pp, !is.finite(humans2wwtp_pp), 0)
    )
  rast_wwtp2water <- df_wwtp %>%
    dplyr::group_by(area_type) %>%
    dplyr::select(wwtp2water_pp, iso, area_type) %>%
    dplyr::group_map(~ {
      terra::subst(
        run$domain$isoraster,
        from = .x$iso,
        to = .x$wwtp2water_pp,
        others = 0,
        names = paste("wwtp2water_pp", .y, sep = "_")
      )
    }) %>%
    terra::rast()

  rast_wwtp2water$wwtp2water_urban <- rast_wwtp2water$wwtp2water_pp_urban *
    run$populations$urban
  rast_wwtp2water$wwtp2water_rural <- rast_wwtp2water$wwtp2water_pp_rural *
    run$populations$rural
  rast_wwtp2water$wwtp2water <- sum(
    rast_wwtp2water$wwtp2water_urban, rast_wwtp2water$wwtp2water_rural,
    na.rm = TRUE
  )

  rast_humans2wwtp <- df_wwtp %>%
    dplyr::group_by(area_type) %>%
    dplyr::select(humans2wwtp_pp, iso, area_type) %>%
    dplyr::group_map(~ {
      terra::subst(
        run$domain$isoraster,
        from = .x$iso,
        to = .x$humans2wwtp_pp,
        others = 0,
        names = paste("humans2wwtp_pp", .y, sep = "_")
      )
    }) %>%
    terra::rast()

  rast_humans2wwtp$humans2wwtp_urban <- rast_humans2wwtp$humans2wwtp_pp_urban *
    run$populations$urban
  rast_humans2wwtp$humans2wwtp_rural <- rast_humans2wwtp$humans2wwtp_pp_rural *
    run$populations$rural
  rast_humans2wwtp$humans2wwtp <- sum(
    rast_humans2wwtp$humans2wwtp_urban, rast_humans2wwtp$humans2wwtp_rural,
    na.rm = TRUE
  )

  rast_wwtp <-
    terra::rast(
      list(
        wwtp2water = rast_wwtp2water$wwtp2water,
        humans2wwtp = rast_humans2wwtp$humans2wwtp
      )
    )
  terra::varnames(rast_wwtp) <- c("wwtp2water", "humans2wwtp")
  terra::units(rast_wwtp) <- "particles/year"

  return(rast_wwtp)
}
