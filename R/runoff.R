#' Runoff Survival
#'
#' @param rast_runoff SpatRaster holding surface runoff (mm)
#' @param retention_lower numeric
#' @param retention_upper numeric
#'
#' @return SpatRaster holding the pathogen survival fractions from land
#' to surface water through surface runoff
#'
runoff_fraction <- function(rast_runoff, retention_lower, retention_upper) {
  rast_retention <-
    terra::app(round(rast_runoff, digits = 1),
               function(runoff_values, lower, upper) {
                 retention_values <-
                   sapply(
                     runoff_values,
                     runoff_retention,
                     retention_lower = lower,
                     retention_upper = upper
                   )
                 return(retention_values)
               }, lower = retention_lower, upper = retention_upper)

  rast_runoff_fraction <-
    terra::app(rast_retention, function(retention_vals) {
      runoff_fraction_vals <- 10 ^ (-retention_vals)
      return(runoff_fraction_vals)
    })
  terra::time(rast_runoff_fraction, tstep = run$settings$hydrology$step) <-
    1:terra::nlyr(rast_runoff_fraction)
  return(rast_runoff_fraction)
}

runoff_retention <- function(runoff, retention_lower, retention_upper) {
  # runoff values above 78.1 will give negative retention values when using 3.2
  # and 8.8 for lower and upper retention values. runoff values above 60 will
  # return retention values below 1.
  retention <- NA

  # this requires some explanation
  if (is.na(runoff)) {
    return(NA)
  }
  # because at zero runoff, retention is infinite, no oocysts are transported
  if (runoff == 0) {
    retention <- Inf
  } else if (runoff < 0.1 && runoff > 0) {
    retention <- 1
  } else if (runoff <= 20) {
    retention <-
      retention_upper - ((runoff / 20) * (retention_upper - retention_lower))
  } else { # > 20
    retention <-
      retention_lower - (((runoff - 20) / 40) * (retention_lower - 1))
    # retention values below 1 does not make sense, because this will result in
    # oocysts growth instead of decay. This can happen when runoff values exceed
    # a certain value.
    if (retention < 1) {
      retention <- 1
    }
  }
  return(retention)
}
