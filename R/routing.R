#' Routing
#'
#' @param rast_inflow SpatRaster holding the pathogen load inflow from land to water
#' @param rast_survival SpatRaster holding the survival fraction
#' @param rast_flowdir SpatRaster holding the flow direction. See details.
#' @param rast_flowacc SpatRaster holding the flow accumulation (ncells) from upstream cells.
#' @details
#' The flowdirection is specified as follows:
#'
#' | 32 | 64 | 128 |
#' |:--:|:--:|:---:|
#' | 16 |  0 |  1  |
#' | 8  |  4 |  2  |
#'
#' @return SpatRaster holding the pathogen loads in rivers.
routing <- function(rast_inflow, rast_survival, rast_flowdir, rast_flowacc) {
  downstream_cel <-
    cellnum <-
    is_outlet <-
    routed <- survival <- inflow <- to_downstream <- new_routed <- NULL
  logger_log("Start routing.", logger::INFO)
  rast_inflow[is.na(rast_inflow)] <- 0
  rast_survival <- terra::subst(rast_survival, from = NA, to = 0)

  # fill gaps
  rast_x_dir <-
    terra::subst(
      rast_flowdir,
      from = c(0, 1, 2, 4, 8, 16, 32, 64, 128),
      to = c(0, 1, 1, 0, -1, -1, -1, 0, 1)
    )
  # raster starts counting in upper left corner
  rast_y_dir <-
    terra::subst(
      rast_flowdir,
      from = c(0, 1, 2, 4, 8, 16, 32, 64, 128),
      to = c(0, 0, 1, 1, 1, 0, -1, -1, -1)
    )
  # x, and y defines next downstream cell
  cells <- terra::cells(rast_flowacc)
  flowacc_cells <- rast_flowacc[cells] %>% dplyr::pull()
  flowdir_cells <- rast_flowdir[cells] %>% dplyr::pull()
  survival_cells <- rast_survival[cells] %>% dplyr::pull()
  df <-
    data.frame(
      cellnum = cells,
      flowacc = flowacc_cells,
      flowdir = flowdir_cells,
      survival = survival_cells
    ) %>%
    dplyr::arrange(flowacc)

  df$row <- terra::rowFromCell(rast_flowacc, df$cellnum)
  df$col <- terra::colFromCell(rast_flowacc, df$cellnum)
  df$downstream_row <- df$row + rast_y_dir[df$cellnum] %>% dplyr::pull()
  df$downstream_col <- df$col + rast_x_dir[df$cellnum] %>% dplyr::pull()
  df$downstream_cel <-
    terra::cellFromRowCol(rast_flowacc, df$downstream_row, df$downstream_col)

  # filter all rows which have a valid downstream cell
  df_is_outlet <- df %>%
    dplyr::filter(!is.na(downstream_cel)) %>%
    # validate the downstream cell. The cell is considered an outlet if the
    # downstream cell does not have a valid flow accumulation value.
    dplyr::mutate(is_outlet = is.na(rast_flowacc[downstream_cel])[, 1] |
                    cellnum == downstream_cel) %>%
    dplyr::select(cellnum, is_outlet)
  # join the is_outlet column to the full data.frame. Set all cells without
  # valid downstream cell to an outlet cell.
  df <- df %>%
    dplyr::full_join(df_is_outlet, by = "cellnum") %>%
    dplyr::mutate(is_outlet =
                    dplyr::case_when(is.na(is_outlet) ~ TRUE,
                                     .default = is_outlet))

  # initialize the routed emissions
  df$inflow <- rast_inflow[df$cellnum]
  df$routed <- array(0, dim = c(nrow(df), terra::nlyr(rast_inflow)))
  unique_flowacc <- unique(df$flowacc)
  # cut off the largest flow accumulation, because it will always be an outlet
  unique_flowacc <- unique_flowacc[-length(unique_flowacc)]
  processed_cells <- 0
  previous_progress <- 0
  total_cells <- length(unique_flowacc)
  routing_progress(0)
  for (flowacc in unique_flowacc) {
    # get the downstream cell and cell emissions
    df_upstream <- df %>%
      dplyr::filter(flowacc == !!flowacc & !is_outlet) %>%
      dplyr::select(cellnum, downstream_cel, inflow, routed, survival) %>%
      dplyr::arrange(downstream_cel)
    # Test if upstream cells are found which have a downstream cell inside
    # the mask.
    if (nrow(df_upstream) > 0) {
      # calculate the emissions to downstream from the pathogen survival
      # in rivers
      df_upstream$to_downstream <-
        (df_upstream$inflow + df_upstream$routed) * df_upstream$survival
      # group by downstream cell, because some cells might flow to the same
      # downstream cell.
      df_upstream_grouped <- df_upstream %>% dplyr::group_by(downstream_cel)
      df_upstream_summed <- df_upstream_grouped %>%
        dplyr::group_map(~ colSums(.x$to_downstream, na.rm = TRUE)) %>%
        unlist() %>%
        array(dim = c(terra::nlyr(rast_inflow), length(unique(
          df_upstream$downstream_cel
        )))) %>%
        t()
      # create new data.frame using the downstream cells as identifier
      df_to_downstream <- dplyr::group_keys(df_upstream_grouped)
      # add the emission load to the downstream cell
      df_to_downstream$to_downstream <- df_upstream_summed

      # join df and calculate the routed emissions from the upstream cell
      df_downstream <- df_to_downstream %>%
        dplyr::left_join(df,
                         by = dplyr::join_by(downstream_cel == cellnum),
                         keep = TRUE) %>%
        dplyr::select(cellnum, routed, to_downstream) %>%
        # add the upstream emissions to downstream cell
        dplyr::mutate(new_routed = routed + to_downstream) %>%
        dplyr::select(cellnum, new_routed) %>%
        dplyr::rename(routed = "new_routed")

      df <- df %>% dplyr::rows_update(df_downstream, by = "cellnum")
      processed_cells <- processed_cells + 1
      progress_val <- (processed_cells / total_cells) * 100
      if ((progress_val - previous_progress) > 5 || progress_val == 100) {
        routing_progress(progress_val)
        previous_progress <- progress_val
      }
    }
  }
  rast_routed <- terra::rast(rast_inflow, vals = 0)
  # rasterize the routed emissions
  rast_routed[df$cellnum] <- df$routed
  # the rast_routed is added to the cell inflow, because the the rast_routed is
  # only the routed part (e.g. emissions from upstream inflow cells) and has to
  # be added to the load of the actual cell.
  rast_outflow <- sum(rast_inflow, rast_routed, na.rm = TRUE)
  rast_outflow <- terra::mask(rast_outflow, run$domain)
  logger_log("Finished routing", logger::INFO)
  return(rast_outflow)
}

routing_progress <- function(progress_val) {
  round_progress <- round(progress_val)
  progress_bar <-
    sprintf(
      "|%s%s|  %.0f%%",
      paste(replicate(round_progress, "="), collapse = ""),
      paste(replicate((100 - round_progress), " "), collapse = ""),
      round_progress
    )
  logger_log(progress_bar, logger::INFO)
}
