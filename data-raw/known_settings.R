
full_dummy_settings <- list(
  logger = list(
    enabled = TRUE,
    threshold = "DEBUG",
    file = "dummy",
    appender = "CONSOLE"
  ),
  input = list(
    isoraster = "dummy",
    isodata = "dummy",
    pathogen = "dummy",
    population = list(
      rural = "dummy",
      urban = "dummy"
    ),
    wwtp = "dummy",
    manure = list(
      management_systems = "dummy"
    ),
    temperature = list(
      year = "dummy"
    ),
    livestock = list(
      production_systems = "dummy",
      manure_fractions = "dummy",
      animal_isoraster = "dummy",
      animals = list(
        asses = list(
          heads = "dummy",
          isodata = "dummy"
        ),
        buffaloes = list(
          heads = "dummy",
          isodata = "dummy"
        ),
        camels = list(
          heads = "dummy",
          isodata = "dummy"
        ),
        cattle = list(
          heads = "dummy",
          isodata = "dummy"
        ),
        chickens = list(
          heads = "dummy",
          isodata = "dummy"
        ),
        ducks = list(
          heads = "dummy",
          isodata = "dummy"
        ),
        goats = list(
          heads = "dummy",
          isodata = "dummy"
        ),
        horses = list(
          heads = "dummy",
          isodata = "dummy"
        ),
        mules = list(
          heads = "dummy",
          isodata = "dummy"
        ),
        pigs = list(
          heads = "dummy",
          isodata = "dummy"
        ),
        sheep = list(
          heads = "dummy",
          isodata = "dummy"
        )
      )
    ),
    hydrology = list(
      runoff = "dummy",
      discharge = "dummy",
      river_temperature = "dummy",
      river_restime = "dummy",
      river_depth = "dummy",
      ssrd = "dummy",
      doc = "dummy"
    ),
    routing = list(
      flowdir = "dummy",
      flowacc = "dummy"
    )
  ),
  pathogen = "dummy",
  wwtp = list(
    treatment = "dummy"
  ),
  livestock = list(
    enabled = "dummy"
  ),
  hydrology = list(
    enabled = "dummy",
    step = "dummy"
  ),
  population = list(
    correct = "dummy"
  ),
  output = list(
    dir = "dummy",
    sources = list(
      human = list(
        land = "dummy",
        surface_water = "dummy"
      ),
      livestock = list(
        land = "dummy",
        surface_water = "dummy"
      )
    ),
    sinks = list(
      surface_water = list(
        table = "dummy",
        grid = "dummy"
      ),
      land = list(
        table = "dummy",
        grid = "dummy"
      ),
      wwtp = list(
        grid = "dummy",
        table = "dummy"
      ),
      manure_storage = list(
        table = "dummy",
        grid = "dummy"
      )
    ),
    hydrology = list(
      loads = "dummy",
      concentration = "dummy"
    )
  ),
  constants = list(
    runoff_fraction = "dummy",
    threshold_discharge = "dummy"
  )
)

known_settings <- names(unlist(full_dummy_settings, recursive = TRUE))
