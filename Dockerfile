FROM rocker/geospatial:4.3.2

RUN apt-get update

ARG GIT_COMMIT
RUN echo "Based on git commit: $GIT_COMMIT"
RUN R -e "devtools::install_git('https://git.wur.nl/glowpa/glowpa-r.git', ref = '$GIT_COMMIT')"