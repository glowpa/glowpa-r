library(glowpa)

test_that("logger uses threshold", {
  logger::log_threshold("INFO")
  expect_false(logger::log_threshold() == logger::WARN)
  run$settings$logger$enabled <- TRUE
  run$settings$logger$threshold <- "WARN"
  run$settings$logger$appender <- "CONSOLE"
  run$settings$logger$file <- NULL
  logger_init(run$settings$logger)
  expect_equal(logger::log_threshold(), logger::WARN)
})

test_that("logger writes to file", {
  log_file <- tempfile("log", fileext = ".log")
  logger_settings <- list(
    appender = "FILE",
    file = log_file,
    enabled = TRUE,
    threshold = "INFO"
  )
  expect_false(file.exists(log_file))
  logger_init(logger_settings)
  run$settings$logger$enabled <- TRUE
  logger_log("test error message", logger::ERROR)
  expect_true(file.exists(log_file))
  log_content <- readLines(log_file)
  expect_match(log_content, "test error message")
})

test_that("logger appends to console",{
  logger_settings <- list(
    appender = "CONSOLE",
    enabled = TRUE,
    threshold = "INFO"
  )
  logger_init(logger_settings)
  run$settings$logger$enabled <- TRUE
  log_msg <- "Test console logging"
  expect_match(capture.output(logger_log(log_msg, logger::INFO),type = "message"), 'console')
})

test_that('logger appends to console and file',{
  log_file <- tempfile("log", fileext = ".log")
  logger_settings <- list(
    appender = "TEE",
    enabled = TRUE,
    threshold = "INFO",
    file = log_file
  )
  logger_init(logger_settings)
  run$settings$logger$enabled <- TRUE
  log_msg <- "Test console logging"
  expect_match(capture.output(logger_log(log_msg, logger::INFO),type = "message"), 'console')
  expect_match(readLines(log_file)[1], 'console')
})
