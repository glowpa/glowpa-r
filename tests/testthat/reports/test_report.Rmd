---
title: "Test report"
output:
  pdf_document:
    df_print: !expr knitr::kable
  html_document:
    df_print: !expr knitr::kable
classoption: landscape
---

## Test Results
```{r setup, include=FALSE}
library(dplyr)
library(kableExtra)
get_cache_time <- function(){
  # get latest modification date
  package_files <- list.files(c("R","data","inst"), recursive = T, full.names = T)
  test_files <- list.files("tests", recursive = T, full.names = T, pattern = ".R$")
  t <- max(file.info(c(package_files, test_files))$mtime)
  return(as.numeric(t))
}
```

```{r run_test, cache=TRUE, cache.extra = get_cache_time() ,include=FALSE}
test_results <- testthat::test_local(reporter = testthat::ListReporter)
df_tests <- test_results %>% as_tibble()
```

```{r print_tests, echo=FALSE}
df_test_summary <-
  df_tests %>% group_by(file, test) %>% 
  mutate(test = stringr::str_trunc(test, 30)) %>% summarise(
    n = sum(nb),
    passed = sum(passed),
    failed = sum(failed),
    skipped = sum(skipped),
    warnings = sum(warning),
    errors = sum(as.numeric(error)),
    time = sum(user) ,
    .groups = "drop"
  )

green <- rgb(col2rgb("green4")[1], col2rgb("green4")[2], col2rgb("green4")[3], maxColorValue = 255)
red <- rgb(col2rgb("red4")[1], col2rgb("red4")[2], col2rgb("red4")[3], maxColorValue = 255)
orange <- rgb(col2rgb("orange2")[1], col2rgb("orange2")[2], col2rgb("orange2")[3], maxColorValue = 255)

default_color <- "black"
if(knitr::is_html_output()){
  default_color   <- ""
}

kbl(df_test_summary, booktabs = TRUE) %>% kable_paper("striped", latex_options = c("striped","hold_position"), position = "left") %>% column_spec(
  1,
  color = ifelse(
    df_test_summary$passed + df_test_summary$skipped  == df_test_summary$n,
    green,
    red
  )
) %>% column_spec(5, color = ifelse(df_test_summary$failed > 0, red, default_color)) %>%
  column_spec(6, color = ifelse(df_test_summary$skipped > 0, orange, default_color))

```

## Code Coverage
```{r code-coverage, cache=TRUE, cache.extra = get_cache_time() ,include=FALSE}
coverage_results <- covr::package_coverage()
coverage_df <- covr::coverage_to_list(coverage_results) %>% 
  as.data.frame() %>% dplyr::select(filecoverage)
```
```{r print-coverage, echo=FALSE}
get_coverage_color <- function(val){
  if(val < 75){
    return(red)
  } else if(val < 90){
    return(orange)
  } else{
    return(green)
  }
}
coverage_table <- coverage_df %>% dplyr::arrange(filecoverage)
kbl(coverage_table, booktabs = TRUE) %>% kable_paper("striped", latex_options = c("striped","hold_position"),position = "left") %>%
  column_spec(1, color = sapply(coverage_table$filecoverage, get_coverage_color))
```

